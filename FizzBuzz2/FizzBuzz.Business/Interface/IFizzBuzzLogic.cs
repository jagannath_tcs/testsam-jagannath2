﻿using System.Collections.Generic;

namespace FizzBuzz.Business
{
    public interface IFizzBuzzLogic
    {
        List<IFizzBuzz> GetFizzBuzzNumbersList(int inputFizzBuzzNumber);
    }
}
