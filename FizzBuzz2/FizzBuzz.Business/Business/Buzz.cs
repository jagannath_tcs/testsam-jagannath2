﻿
namespace FizzBuzz.Business
{
    public class Buzz : IFizzBuzz
    {
        public bool IsNumberDivisible(int inputFizzBuzzNumber)
        {
            return (inputFizzBuzzNumber % 5) == 0;
        }

        public string MessageStyle
        {
            get { return "green"; }
        }

        public string DisplayMessage { get; set; }

        public void SetDisplayMessage(bool isTodayWednesday)
        {
            this.DisplayMessage = isTodayWednesday ? "Wuzz" : "Buzz";
        }
    }
}
