﻿
namespace FizzBuzz2.Business.Tests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using NUnit.Framework;
    using FluentAssertions;
    using FizzBuzz.Business;

    [TestClass]
    public class BuzzUniTest
    {
        private Buzz buzz;
        private string expectedResult;

        /// <summary>
        /// Tests to initialize the buzz logic.
        /// </summary>
        [SetUp]
        public void TestInitialize()
        {
            this.buzz = new Buzz();
            this.expectedResult = "green";
        }

        [TestCase(10)]
        public void TestCheckNumberDivisibleBy5(int inputFizzBuzzNumber)
        {
            var actualResult = this.buzz.IsNumberDivisible(inputFizzBuzzNumber);
            actualResult.ShouldBeEquivalentTo(true);
        }

        [TestCase]
        public void TestGetBuzzMessageStyle()
        {
            var actualResult = this.buzz.MessageStyle;
            actualResult.ShouldBeEquivalentTo(this.expectedResult);
        }
    }
}
