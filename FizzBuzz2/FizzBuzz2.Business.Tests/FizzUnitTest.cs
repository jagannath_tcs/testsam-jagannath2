﻿

namespace FizzBuzz2.Business.Tests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using NUnit.Framework;
    using FluentAssertions;
    using FizzBuzz.Business;


    [TestClass]
    public class FizzUnitTest
    {
        private Fizz fizz;
        private string expectedResult;

        /// <summary>
        /// Tests the initialize the fizz logic.
        /// </summary>
        [SetUp]
        public void TestInitialize()
        {
            this.fizz = new Fizz();
            this.expectedResult = "blue";
        }

        [TestCase(9)]
        public void TestCheckNumberDivisibleBy3(int inputFizzBuzzNumber)
        {
            var actualResult = this.fizz.IsNumberDivisible(inputFizzBuzzNumber);
            actualResult.ShouldBeEquivalentTo(true);
        }

        [TestCase]
        public void TestGetFizzMessageStyle()
        {
            var actualResult = this.fizz.MessageStyle;
            actualResult.ShouldBeEquivalentTo(this.expectedResult);
        }
    }
}
