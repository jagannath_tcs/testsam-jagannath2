﻿
namespace FizzBuzz2.Business.Tests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using NUnit.Framework;
    using FizzBuzz.Business;
    using FluentAssertions;

    [TestClass]
    public class FizzBuzzHelperUnitTest
    {
        private Mock<IDayProvider> dayProvider;
        private Mock<IFizzBuzzLogic> mockFizzBuzzLogic;
        private TestHelper fizzbuzzTestHelper;
        private string expectedResult;

        /// <summary>
        /// Tests to initialize the fizzbuzz logic.
        /// </summary>
        [SetUp]
        public void TestInitialize()
        {
            this.mockFizzBuzzLogic = new Mock<IFizzBuzzLogic>();
            this.dayProvider = new Mock<IDayProvider>();
            this.fizzbuzzTestHelper = new TestHelper();
        }

        [TestCase(15)]
        public void TestGetListOfFizzBuzzNumbers(int inputFizzBuzzNumber)
        {
            this.dayProvider.Setup(x => x.IsTodayWednesday()).Returns(false);
            var fizzbuzzLogic = new FizzBuzzHelper(this.fizzbuzzTestHelper.GetMockFizzBuzzList(14), this.dayProvider.Object);
            this.fizzbuzzTestHelper.SetExpectedList();
            this.mockFizzBuzzLogic.Setup(x => x.GetFizzBuzzNumbersList(inputFizzBuzzNumber)).Returns(this.fizzbuzzTestHelper.ExpectedResult);

            var resultList = fizzbuzzLogic.GetFizzBuzzNumbersList(inputFizzBuzzNumber);
            for (var counter = 0; counter < inputFizzBuzzNumber; counter++)
            {
                var actualResult = resultList[counter].DisplayMessage;
                actualResult.ShouldBeEquivalentTo(this.fizzbuzzTestHelper.ExpectedResult[counter].DisplayMessage);
            }
        }

        [TestCase(15)]
        public void TestArgumentShouldReplaceDivisibleby3And5WithProperMessage(int inputFizzBuzzNumber)
        {
            this.dayProvider.Setup(x => x.IsTodayWednesday()).Returns(false);
            this.expectedResult = "Fizz Buzz";
            var fizzBuzzLogic = new FizzBuzzHelper(this.fizzbuzzTestHelper.GetMockFizzBuzzList(14), this.dayProvider.Object);
            var list = fizzBuzzLogic.GetFizzBuzzNumbersList(inputFizzBuzzNumber);
            var actualResult = list[14].DisplayMessage;
            actualResult.ShouldBeEquivalentTo(this.expectedResult);
        } 

    }
}
