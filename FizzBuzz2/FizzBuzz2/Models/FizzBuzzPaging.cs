﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FizzBuzz2.Models
{
    public class FizzBuzzPaging
    {
        public FizzBuzzPaging()
        {
            this.PageIndex = 1;
        }

        public int PageIndex { get; set; }
        public int PageCount { get; set; }
    }
}