﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using FizzBuzz.Business;

namespace FizzBuzz2.Models
{
    public class FizzBuzzModel
    {
        public const int PageSize = 20;
        private int? fizzbuzznumber;
        public FizzBuzzPaging fizzbuzzPaging { get; set; }

        public FizzBuzzModel()
        {
            this.fizzbuzzPaging = new FizzBuzzPaging();
        }

        [Required]
        [Display(Name = "Please enter the number")]
        [Range(1, 1000, ErrorMessage = "Please enter a value between 1 and 1000")]
        public int? FizzBuzzNumber
        {
            get
            {
                return this.fizzbuzznumber;
            }

            set
            {
                this.fizzbuzznumber = value;
                this.fizzbuzzPaging.PageIndex = 1;
                this.fizzbuzzPaging.PageCount = Convert.ToInt32(this.fizzbuzznumber) / PageSize;
                this.fizzbuzzPaging.PageCount = (this.fizzbuzznumber % PageSize) == 0 ? this.fizzbuzzPaging.PageCount : this.fizzbuzzPaging.PageCount + 1;
            }
        }

        public List<IFizzBuzz> FizzBuzzNumberList { get; set; }
    }
}