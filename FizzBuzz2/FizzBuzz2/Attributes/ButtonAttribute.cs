﻿
using System.Reflection;
using System.Web.Mvc;

namespace FizzBuzz2.Attributes
{
    public class ButtonAttribute : ActionMethodSelectorAttribute
    {
        public string Name { get; set; }

        public override bool IsValidForRequest(ControllerContext controllerContext, MethodInfo methodInfo)
        {
            return controllerContext.Controller.ValueProvider.GetValue(this.Name) != null;
        }
    }
}