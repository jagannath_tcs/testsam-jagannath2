﻿using FizzBuzz2.Models;

namespace FizzBuzz2.Mappers
{
    public class FizzBuzzPagingMapper
    {
        public static void SetFizzBuzzListPaging(FizzBuzzModel fizzbuzzModel, string caseSwitchPaging, object tempdataFizzBuzzPage)
        {
            switch (caseSwitchPaging)
            {
                case "next":
                    fizzbuzzModel.fizzbuzzPaging = tempdataFizzBuzzPage != null
                                             ? tempdataFizzBuzzPage as FizzBuzzPaging
                                             : new FizzBuzzPaging();
                    if (fizzbuzzModel.fizzbuzzPaging != null)
                    {
                        fizzbuzzModel.fizzbuzzPaging.PageIndex = fizzbuzzModel.fizzbuzzPaging.PageIndex == fizzbuzzModel.fizzbuzzPaging.PageCount
                                                           ? fizzbuzzModel.fizzbuzzPaging.PageIndex
                                                           : fizzbuzzModel.fizzbuzzPaging.PageIndex + 1;
                    }

                    break;
                case "previous":
                    fizzbuzzModel.fizzbuzzPaging = tempdataFizzBuzzPage != null
                        ? tempdataFizzBuzzPage as FizzBuzzPaging
                        : new FizzBuzzPaging();
                    if (fizzbuzzModel.fizzbuzzPaging != null)
                    {
                        fizzbuzzModel.fizzbuzzPaging.PageIndex = fizzbuzzModel.fizzbuzzPaging.PageIndex == 1
                            ? fizzbuzzModel.fizzbuzzPaging.PageIndex
                            : fizzbuzzModel.fizzbuzzPaging.PageIndex - 1;
                    }

                    break;
                default:
                    fizzbuzzModel.fizzbuzzPaging.PageIndex = 1;
                    break;
            }
        }
    }
}